<?php
require_once('Registry/grid.php');
/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/29/16
 * Time: 12:01 AM
 */
class mainFrame
{
    private $playerLabels = array();
    private $playerLabelThatWon;

    public function setPlayerLabels($label)
    {
        $this->playerLabels[]=$label;
    }

    public function main($numberOfPlayers,$fieldSize,$numberOfLabelsToWin)
    {
        if ($numberOfLabelsToWin<=$fieldSize){
            $grid = new grid($fieldSize,$numberOfLabelsToWin);
        } else {
            echo "Field size shall not be smaller than number of players<br>";
            return 0;
        }
        while ($grid->getVictory()==0||$grid->getGameOver()==0) {  // Game time
            for ($i = 0; $i < $numberOfPlayers; $i++) {
                $grid->move($this->playerLabels[$i]);
                if ($grid->getVictory() || $grid->checkGameOver()) {  // Check if someone won after move
                    $this->playerLabelThatWon = $this->playerLabels[$i];
                    break 2;
                }
            }
        } if($grid->getVictory()) {
            echo "Player " . $this->playerLabelThatWon . " has won.";
            $grid->drawGrid();
        } else {
            echo "Game over, no one won<br>";
        }
        return 0;
    }
}

$main= new mainFrame();
$main->setPlayerLabels('a');
$main->setPlayerLabels('b');
$main->setPlayerLabels('c');
$main->setPlayerLabels('d');
$main->main(4,40,5);
