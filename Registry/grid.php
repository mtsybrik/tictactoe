<?php

/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 9/29/16
 * Time: 12:02 AM
 */
class grid
{
    private $field=array();
    private $victory = false;
    private $counter=0;
    private $gameOver=false;
    private $numberOfLabelsToWin;
    private $fieldSize;
    private $lastRemoval;

    public function __construct($fieldSize,$numberOfLabelsToWin)
    {
        for ($x=0;$x<$fieldSize;$x++) {
            for ($y=0;$y<$fieldSize;$y++) {
                $this->field[$x][$y]='';
            }
        }
        $this->fieldSize = $fieldSize;
        $this->numberOfLabelsToWin=$numberOfLabelsToWin;
    }

    public function getVictory()
    {
        return $this->victory;
    }

    public function getGameOver()
    {
        return $this->gameOver;
    }
    public function setField($field)
    {
        $this->field=$field;
    }

    private function checkArrayForVictory($array,$playerLabel)
    {
        $labelCounter=0;
        for ($yAxis=0;$yAxis<=$this->fieldSize;$yAxis++) {
            if (isset($array[$yAxis])&&$array[$yAxis] === $playerLabel) {
                $labelCounter++;
            } else {
                $labelCounter = 0;
            } if ($labelCounter == $this->numberOfLabelsToWin) {
                $this->victory = 1;
                return 1;
            }
        }
        return 0;
    }

    public function move($playerLabel)
    {
        $b=0;
        while(!$b) {
            $x = rand(0, $this->fieldSize);
            $y = rand(0, $this->fieldSize);
            if (isset($this->field[$x][$y]) && $this->field[$x][$y] == "") {
                // ...place label on first available field
                $this->field[$x][$y] = $playerLabel;
                $this->checkVictory($x,$y,$playerLabel);
                $b=1;
            }
        }
    }

    public function checkGameOver()     // Method to check if all fields are filled
    {
        $this->counter=0;
        for ($xAxis=0; $xAxis<=$this->fieldSize; $xAxis++) {
            for ($yAxis=0; $yAxis<=$this->fieldSize; $yAxis++) {
                if (isset($this->field[$xAxis][$yAxis])&&$this->field[$xAxis][$yAxis]!='') {
                    $this->counter++;
                }
            }
        }
        if ($this->counter==$this->fieldSize*$this->fieldSize) {
            $this->gameOver=1;
            return 1;
        }
        return 0;
    }

    public function checkVictory($x,$y,$playerLabel)
    {
        $horizontalArray=$this->field[$x];
        $verticalArray=array_column($this->field,$y);
        $diagonal=array();
        $antidiagonal=array();
        if($this->checkArrayForVictory($horizontalArray,$playerLabel)) { //Horizontal check
            return 1;
        } if($this->checkArrayForVictory($verticalArray,$playerLabel)) { // Vertical check
            return 1;
        }
        if ($x>=$y) {
            $h=$x-$y;
            for($i=$h;$i<=$this->fieldSize;$i++) {
                if(isset($this->field[$i][$i-$h])) {
                    $diagonal[]=$this->field[$i][$i-$h]; //fill in diagonal array if cell is above middle
                }
            }
        } if($x<$y) {
            $h=$y-$x;
            for($i=$h;$i<=$this->fieldSize;$i++) {
                if(isset($this->field[$i - $h][$i])) {
                    $diagonal[] = $this->field[$i - $h][$i]; //fill in diagonal array if cell is below middle
                }
            }
        } if($this->checkArrayForVictory($diagonal,$playerLabel) ) { // Diagonal check
            return 1;
        } if($x+$y<=$this->fieldSize) {
            $j=0;
            for($i=$x+$y;$i>=0;$i--) {
                if(isset($this->field[$i][$j])) {
                    $antidiagonal[] = $this->field[$i][$j]; //fill in antidiagonal array if cell is above middle
                }
                $j++;
            }
        } if($x+$y>$this->fieldSize){
            $j=$this->fieldSize;
            for($i=$x+$y-$this->fieldSize;$i<=$this->fieldSize;$i++) {
                if(isset($this->field[$i][$j])) {
                    $antidiagonal[] = $this->field[$i][$j]; //fill in antidiagonal array if cell is below middle
                }
                $j--;
            }
        } if($this->checkArrayForVictory($antidiagonal,$playerLabel)) { // Antidiagonal check
            return 1;
        }
        return 0;
    }

    public function drawGrid()
    {
        echo '<style>
        tr:nth-child(even) {background-color: #f5f5f5}
        th, td {
            padding: 5px;
            text-align: center;
            }
        th {
            border: 1px solid #00869D;
            color:#00869D;
            background-color:white;
        }
        td {
            border: 1px dotted #00869D;
        }

        </style>
            <div style="overflow-x:auto; "><table align="center" style="border-collapse: collapse; border: 1px solid #00869D; width:auto;">';
        foreach ($this->field as $key => $line) {
            echo "<tr>";
            foreach ($line as $k => $cell) {
                echo "<td>" . $cell . "</td>";
            }
        }
        echo '</table>';
    }

    public function removeOpponentsMark($x,$y,$playerLabel)
    {
        if($this->lastRemoval!=$playerLabel) {
            if (isset($this->field[$x][$y]) && $this->field[$x][$y] != "") {
                $this->field[$x][$y] = "";
                $this->lastRemoval = $playerLabel;
                return 1;
            }
        }
        return 0;
    }
}