<?php
require_once('grid.php');
/**
 * Created by PhpStorm.
 * User: ntsybrik
 * Date: 1 ""/8/16
 * Time: 5:4 "" PM
 */
class tests
{
    private $fieldWithHorizontalVictory=[[ "",1, "", ""],[ "", "", 2, ""],[ "", 2, "",1],[ "",1,1,1]];
    private $fieldWithVerticalVictory=[[ "", "",1],[ "", "",1],[ "", "",1]];
    private $fieldWithLeftToRightDiagonalVictory=[[1, "", "", ""],[ "",1, "", ""],[ "", "",1, ""],[ "", "", "",1]];
    private $fieldWithLeftToRightBelowMiddleDiagonalVictory=[[ "", "", "", ""],[1, "", "", ""],[ "",1, "", ""],[ "", "",1, ""]];
    private $fieldWithRightToLeftDiagonalVictory=[[ "", "",1],[ "",1, ""],[1, "", ""]];
    private $fieldWithRightToLeftBelowMiddleDiagonalVictory=[[ "", "", "", ""],[ "", "", "",1],[ "", "",1, ""],[ "",1, "", ""]];
    private $numberOfLabelsToWin=3;
    private $fieldSize=3;

    public function horizontalVictoryTest()
    {
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithHorizontalVictory);
        if($grid->checkVictory(3,1,1)){
            echo "Horizontal victory checker works<br>";
        } if($grid->checkVictory(2,3,1)){
            echo "Horizontal victory checker is broken<br>";
        }
        
    }
    public function verticalVictoryTest()
    {
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithVerticalVictory);
        if($grid->checkVictory(0,2,1)){
            echo "Vertical victory checker works<br>";
        } if($grid->checkVictory(0,1,1)){
            echo "Vertical victory checker is broken<br>";
        }
    }
    public function diagonalLeftToRightVictoryTest()
    {
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithLeftToRightDiagonalVictory);
        if($grid->checkVictory(2,2,1)){
            echo "Diagonal left to right works<br>";
        }
        if($grid->checkVictory(2,1,1)){
            echo "Diagonal left to right is broken<br>";
        }
    }

    public function diagonalRightToLeftVictoryTest()
    {
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithRightToLeftDiagonalVictory);
        if($grid->checkVictory(2,0,1)){
            echo "Diagonal right to left victory checker works<br>";
        } if($grid->checkVictory(2,1,1)){
            echo "Diagonal right to left victory checker is broken<br>";
        }
    }

    public function diagonalLeftToRightBelowMiddleVictoryTest()
    {
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithLeftToRightBelowMiddleDiagonalVictory);
        if($grid->checkVictory(2,1,1)){
            echo "Diagonal left to right below middle victory checker works<br>";
        } if($grid->checkVictory(2,2,1)){
            echo "Diagonal left to right below middle victory checker is broken<br>";
        }
    }

    public function diagonalRightToLeftBelowMiddleVictoryTest()
    {
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithRightToLeftBelowMiddleDiagonalVictory);
        if($grid->checkVictory(3,1,1)){
            echo "Diagonal right to left below middle victory checker works<br>";
        } if($grid->checkVictory(3,2,1)){
            echo "Diagonal right to left below middle victory checker is broken<br>";
        }
    }

    public function check_opponent_removal(){
        $grid=new grid($this->fieldSize,$this->numberOfLabelsToWin);
        $grid->setField($this->fieldWithHorizontalVictory);
        if($grid->removeOpponentsMark(0,1,1)){
            echo "Label removal works<br>";
        }
        if(!$grid->removeOpponentsMark(2,3,1)){
            echo "Second label removal validation works<br>";
        }
        if(!$grid->removeOpponentsMark(2,1,2)){
            echo "Label removal is broken<br>";
        }
        if($grid->removeOpponentsMark(1,2,2)){
            echo "Second time label removal validation is broken<br>";
        }
    }
}

$tests = new tests();
$tests->horizontalVictoryTest();
$tests->verticalVictoryTest();
$tests->diagonalLeftToRightVictoryTest();
$tests->diagonalRightToLeftVictoryTest();
$tests->diagonalLeftToRightBelowMiddleVictoryTest();
$tests->diagonalRightToLeftBelowMiddleVictoryTest();
$tests->check_opponent_removal();